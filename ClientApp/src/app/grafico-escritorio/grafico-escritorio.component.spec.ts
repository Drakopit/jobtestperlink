import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoEscritorioComponent } from './grafico-escritorio.component';

describe('GraficoEscritorioComponent', () => {
  let component: GraficoEscritorioComponent;
  let fixture: ComponentFixture<GraficoEscritorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoEscritorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoEscritorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
