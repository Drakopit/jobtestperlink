import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-grafico-escritorio',
  templateUrl: './grafico-escritorio.component.html',
  styleUrls: ['./grafico-escritorio.component.css']
})
export class GraficoEscritorioComponent implements OnInit {
  @Input()
  constructor() { }

  ngOnInit() {
  }

  chart = new Chart({
    chart: {
      type: 'line'
    },
    title: {
      text: 'Linechart'
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: 'Line 1',
        data: [1, 2, 3]
      }
    ]
  });
}
