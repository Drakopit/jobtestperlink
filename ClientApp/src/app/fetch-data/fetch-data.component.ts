import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ProcessoService } from '../processo.service';
import { Processo } from '../Models/Processo';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public processos: Array<Processo>;

  constructor(private ProcessoService: ProcessoService) {}

  /**
   * getProcessos
   */
  public getProcessos() {
    this.ProcessoService.getProcesso().subscribe((response) => this.processos = response);
  }
}