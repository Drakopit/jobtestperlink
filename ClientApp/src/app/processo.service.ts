import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Processo } from './Models/Processo';

@Injectable({
  providedIn: 'root'
})
export class ProcessoService {

  constructor(private http: HttpClient) { }

  public getProcesso() {
    return this.http.get<Array<Processo>>(`${environment.baseUrl}processo/lista`);
  }

  public postProcesso(Processo: any) {
    return this.http.post<Array<Processo>>(`${environment.baseUrl}processo/salvar`, Processo);
  }
}
