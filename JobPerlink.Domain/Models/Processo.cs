﻿using System;

namespace JobPerlink.Domain.Models
{
    public class Processo : Entity
    {
        public string NumeroDoProcesso { get; set; }
        public DateTime DataDeCriacaoDoProcesso { get; set; }
        public decimal Valor { get; set; }
        public string Escritorio { get; set; }
    }
}
