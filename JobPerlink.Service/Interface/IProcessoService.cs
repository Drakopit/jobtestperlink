﻿using JobPerlink.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPerlink.Service.Interface
{
    public interface IProcessoService : IBaseService<Processo>
    {
        Task<IList<Processo>> GetByEscritorio(string nome);
    }
}
