﻿using JobPerlink.Domain.Models;
using JobPerlink.Repository.Interface;
using JobPerlink.Service.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPerlink.Service
{
    public class ProcessoService : BaseService<Processo>, IProcessoService
    {
        private readonly IProcessoRepository repository;

        public ProcessoService(IProcessoRepository repository) : base(repository)
        {
            this.repository = repository;
        }

        public async Task<IList<Processo>> GetByEscritorio(string nome)
        {
            return await this.repository.GetByCondition(e => e.Escritorio == nome);
        }
    }
}
