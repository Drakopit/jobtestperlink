using AutoMoq;
using Bogus;
using JobPerlink.Domain.Models;
using JobPerlink.Repository.Interface;
using JobPerlink.Service;
using JobPerlink.Service.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace JobPerlink.Test
{
    [CollectionDefinition(nameof(ProcessoCollection))]
    public class ProcessoCollection : ICollectionFixture<ProcessoTestFixture> { }

    public class ProcessoTestFixture : IDisposable
    {
        public Mock<IProcessoRepository> ProcessoRepositoryMock { get; set; }
        public Mock<IProcessoService> ProcessoServiceMock { get; set; }

        public ProcessoService GetProcessoService()
        {
            var mocker = new AutoMoqer();
            mocker.Create<ProcessoService>();

            var processoService = mocker.Resolve<ProcessoService>();

            ProcessoRepositoryMock = mocker.GetMock<IProcessoRepository>();
            ProcessoServiceMock = mocker.GetMock<IProcessoService>();

            return processoService;
        }

        public IEnumerable<Processo> GetLista()
        {
            return GerarProcesso(50).ToList();
        }

        public Processo GetProcesso()
        {
            return GerarProcesso(1).FirstOrDefault();
        }

        public static IEnumerable<Processo> GerarProcesso(int quantidade)
        {
            var fieldIndexTest = new Faker<Processo>("pt_BR")
                .CustomInstantiator(s => new Processo()
                {
                    Id = s.UniqueIndex,
                    DataDeCriacaoDoProcesso = Convert.ToDateTime(s.Date),
                    NumeroDoProcesso = s.UniqueIndex.ToString(),
                    Valor = s.Finance.Amount(),
                    Escritorio = s.Name.ToString()
                });
            return fieldIndexTest.Generate(quantidade);
        }

        public void Dispose()
        {
            // Dispose alguma coisa.
        }
    }
}
