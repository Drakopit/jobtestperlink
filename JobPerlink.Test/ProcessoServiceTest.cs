﻿using FluentAssertions;
using Moq;
using Xunit;

namespace JobPerlink.Test
{
    [Collection(nameof(ProcessoCollection))]
    public class ProcessoServiceTest
    {
        public ProcessoTestFixture Fixture { get; set; }

        public ProcessoServiceTest(ProcessoTestFixture fixture)
        {
            Fixture = fixture;
        }

        [Fact(DisplayName = "Receber Todos os Processos")]
        [Trait("Category", "Processos Tests")]
        public void VerificaSeProcessoNaoEstaVazia()
        {
            // Arrange
            var processos = Fixture.GetLista();

            // Act

            // Assert Fluent Assertions
            processos.Should().NotBeEmpty();

            // Assert xUnit
            Assert.NotEmpty(processos);
        }

        [Fact(DisplayName = "Recebe um Processo")]
        [Trait("Category", "Processos Tests")]
        public void VerificaSeExisteAlgumProcesso()
        {
            // Arrange
            var processo = Fixture.GetProcesso();

            // Act

            // Assert Fluent Assertions
            processo.Should().NotBeNull();

            // Assert xUnit
            Assert.NotNull(processo);
        }

        [Fact(DisplayName = "Adiciona um novo Processo")]
        [Trait("Category", "Processos Tests")]
        public void VerificaSeNovoProcessoFoiAdicionado()
        {
            // Arrannge
            var processoService = Fixture.GetProcessoService();
            var processo = Fixture.GetProcesso();

            // Act
            processoService.Post(processo);

            // Assert
            Fixture.ProcessoRepositoryMock.Verify(s => s.Post(processo), Times.Once);
        }

        [Fact(DisplayName = "Altera um Processo")]
        [Trait("Category", "Processos Tests")]
        public void VerificaSeProcessoFoiAlterada()
        {
            //Arrange
            var processoService = Fixture.GetProcessoService();
            var processo = Fixture.GetProcesso();
            processo.NumeroDoProcesso = "jioasjdioajdo12312321";

            //Act
            processoService.Put(processo);

            //Assert
            Fixture.ProcessoRepositoryMock.Verify(s => s.Put(processo), Times.Once);
        }

        [Fact(DisplayName = "Exclui uma Processo")]
        [Trait("Category", "Processos Tests")]
        public void VerificaSeProcessoFoiRemovida()
        {
            // Arrange
            var processoService = Fixture.GetProcessoService();
            var processo = Fixture.GetProcesso();

            // Act
            processoService.Delete(processo.Id);

            // Assert
            Fixture.ProcessoRepositoryMock.Verify(s => s.Delete(processo.Id), Times.Once);
        }
    }
}
