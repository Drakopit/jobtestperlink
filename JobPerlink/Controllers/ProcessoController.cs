﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JobPerlink.Service.Interface;
using JobPerlink.Domain.Models;

namespace JobPerlink.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProcessoController : ControllerBase
    {
        private readonly IProcessoService service;

        public ProcessoController(IProcessoService service)
        {
            this.service = service;
        }

        [HttpPost("salvar")]
        public async Task<IActionResult> Post(Processo processo) {
            try
            {
                var process = processo;
                await this.service.Post(processo);
                return Ok(process);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id) {
            try
            {
                var processo = await this.service.GetById(id);
                return Ok(processo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("escritorio/{nome}")]
        public async Task<IActionResult> GetByEscritorio(string nome)
        {
            try
            {
                var processosPorEscritorio = await this.service.GetByEscritorio(nome);
                return Ok(processosPorEscritorio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpGet("lista")]
        public async Task<IActionResult> GetList() {
            try
            {
                var processoList = await this.service.Get();
                return Ok(processoList);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }
}
