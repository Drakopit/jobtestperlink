﻿using JobPerlink.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace JobPerlink.Repository
{
    public class PerlinkContext : DbContext
    {
        public DbSet<Processo> Processo { get; set; }

        public PerlinkContext(DbContextOptions<PerlinkContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
