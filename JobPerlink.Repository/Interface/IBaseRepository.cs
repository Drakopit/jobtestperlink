﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JobPerlink.Repository.Interface
{
    public interface IBaseRepository<T> where T : class
    {
        Task<IList<T>> Get();
        Task<IList<T>> GetByCondition(Expression<Func<T, bool>> where);
        Task Post(T entity);
        Task Put(T entity);
        Task Delete(int id);
        Task DeleteList(List<T> entities);
    }
}
