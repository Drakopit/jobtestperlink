﻿using JobPerlink.Domain.Models;

namespace JobPerlink.Repository.Interface
{
    public interface IProcessoRepository : IBaseRepository<Processo> {}
}
