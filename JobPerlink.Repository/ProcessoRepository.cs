﻿using JobPerlink.Domain.Models;
using JobPerlink.Repository.Interface;

namespace JobPerlink.Repository
{
    public class ProcessoRepository : BaseRepository<Processo>, IProcessoRepository
    {
        private readonly PerlinkContext context;
        public ProcessoRepository(PerlinkContext context) : base(context)
        {
            this.context = context;
        }

    }
}
